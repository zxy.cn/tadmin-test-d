// import axios from 'axios'
import instance from './filter';

const http = "http://localhost:5284/api";


//获取token登录
export const getToken = (name: string, password: string) => {
    return instance.get(http + "/Login/GetToken?name=" + name + "&password=" + password);
}


//获取列表
export const getMenuDataNew = async (parms: {}) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.post(http + "/Menu/GetMenus", parms)
}

//添加
export const addMenu = async (parms: {}) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.post(http + "/Menu/Add", parms)
}
//修改
export const editMenu = async (parms: {}) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.post(http + "/Menu/Edit", parms)
}

//删除
export const delMenu = async (id: number) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.get(http + "/Menu/Del?id=" + id)
}

//批量删除
export const batchDelMenu = async (ids: string) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.get(http + "/Menu/BatchDel?ids=" + ids)
}

//设置菜单分配
export const settingMenu = (rid: string, mids: string) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.get(`${http}/Menu/SettingMenu?rid=${rid}&mids=${mids}`)
}

//角色列表模块
//获取角色列表
export const getRoleData = (parms: {}) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.post(http + "/Role/GetRoles", parms)
}
//添加角色
export const addRole = async (parms: {}) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.post(http + "/Role/Add", parms)
}
//编辑
export const editRole = async (parms: {}) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.post(http + "/Role/Edit", parms)
}
//删除
export const delRole = (id: number) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.get(http + "/Role/Del?id=" + id)
}
//批量删除
export const batchDelRole = (ids: string) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.get(http + "/Role/BatchDel?ids=" + ids)
}


//用户模块
//获取列表
export const getUserData = async (parms: {}) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.post(http + "/Users/GetUsers", parms)
}
//添加
export const addUsers = async (parms: {}) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.post(http + "/Users/Add", parms)
}
//修改
export const editUsers = async (parms: {}) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.post(http + "/Users/Edit", parms)
}
//删除
export const delUsers = async (id: number) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.get(http + "/Users/Del?id=" + id)
}
//BatchDel
export const batchDelUsers = async (ids: string) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.get(http + "/Users/BatchDel?ids=" + ids)
}
//分配
export const settingRole = async (pid: string, rids: string) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.get(`${http}/Users/SettingRole?pid=${pid}&rids=${rids}`)
}

//根据角色获取菜单
export const getUserMenus = async () => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.get(`${http}/Menu/GetUserMenus`)
}

//个人中心修改呢称和密码
export const editNickNameOrPassword = async (nickName: string, password: string) => {
    instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage["token"];
    return instance.get(`${http}/Users/EditNickNameOrPassword?nickName=${nickName}&password=${password}`)
}
